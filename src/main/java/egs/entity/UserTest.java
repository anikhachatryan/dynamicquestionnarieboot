package egs.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@Entity
@Table(name = "user_test")
public class UserTest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, updatable = false, insertable = false)
    private Long id;

    @JoinColumn(name = "user_id", nullable = false)
    @ManyToOne()
    private User user;

    @JoinColumn(name = "test_id", nullable = false)
    @ManyToOne()
    private Test test;

    @Column(name = "result", nullable = false)
    private int result;

    @Temporal(TemporalType.DATE)
    @Column(name = "startDate", nullable = false)
    private LocalDate startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "endDate", nullable = false)
    private LocalDate endDate;
}

package egs.entity.enums;

public enum FieldType {
    TEXTAREA,
    CHECKBOX,
    SELECTBOX
}

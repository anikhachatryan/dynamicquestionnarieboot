package egs.dao;

import egs.entity.Question;

import java.util.List;

public interface QuestionDao {

    void add(Question question);

    Question update(Question question);

    void delete(int id);

    Question get(int id);

    List<Question> getAll();
}

package egs.dao;

import egs.entity.UserTest;

import java.util.List;

public interface UserTestDao {

    void add(UserTest admin);

    UserTest update(UserTest admin);

    void delete(int id);

    UserTest get(int id);

    List<UserTest> getAll();

}

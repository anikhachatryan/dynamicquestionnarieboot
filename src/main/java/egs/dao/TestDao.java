package egs.dao;


import egs.entity.Test;
import egs.entity.User;

import java.util.List;

public interface TestDao {
    void add(Test test);

    void delete(int id);

    Test update(Test test);

    Test get(int id);

    List<Test> getAll();

    List<Test> getAllByUser(Long id);
}

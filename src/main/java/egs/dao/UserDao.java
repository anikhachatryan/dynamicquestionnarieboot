package egs.dao;

import egs.entity.User;
//import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserDao {

    void add(User user);

    User update(User user);

    void delete(int id);

    User get(int id);

    List<User> getAll();

    User getByEmail(String email);
}

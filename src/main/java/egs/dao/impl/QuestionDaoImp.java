package egs.dao.impl;

import egs.dao.QuestionDao;
import egs.entity.Question;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuestionDaoImp implements QuestionDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void add(Question question) {
        sessionFactory.getCurrentSession().saveOrUpdate(question);
    }

    @Override
    public void delete(int id) {
        Question question = sessionFactory.getCurrentSession().load(Question.class, id);
        if (question != null) {
            this.sessionFactory.getCurrentSession().delete(question);
        }
    }

    @Override
    public List<Question> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Question").list();
    }

    @Override
    public Question update(Question question) {
        sessionFactory.getCurrentSession().update(question);
        return question;
    }

    @Override
    public Question get(int id) {
        return sessionFactory.getCurrentSession().get(Question.class, id);
    }
}

package egs.dao.impl;

import egs.dao.AdminDao;
import egs.entity.Admin;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdminDaoImp implements AdminDao {

    private SessionFactory sessionFactory;

    @Autowired
    public AdminDaoImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void add(Admin admin) {
        sessionFactory.getCurrentSession().saveOrUpdate(admin);
    }

    @Override
    public void delete(int id) {
        Admin admin = sessionFactory.getCurrentSession().load(Admin.class, id);
        if (admin != null) {
            this.sessionFactory.getCurrentSession().delete(admin);
        }
    }

    @Override
    public List<Admin> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Admin").list();
    }

    @Override
    public Admin update(Admin admin) {
        sessionFactory.getCurrentSession().update(admin);
        return admin;
    }

    @Override
    public Admin get(int id) {
        return (Admin) sessionFactory.getCurrentSession().get(Admin.class, id);
    }

    @Override
    public Admin getByEmail(String email) {
        Query query = sessionFactory.getCurrentSession().createQuery("from admin where email = :email");
        query.setParameter("email", email);
        return (Admin) query.list().get(0);
    }
}

package egs.dao.impl;

import egs.dao.UserTestDao;
import egs.entity.Admin;
import egs.entity.UserTest;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UserTestDaoImp implements UserTestDao {

    private SessionFactory sessionFactory;

    @Autowired
    public UserTestDaoImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    @Override
    public void add(UserTest userTest) {
        sessionFactory.getCurrentSession().saveOrUpdate(userTest);
    }

    @Override
    public UserTest update(UserTest userTest) {
        sessionFactory.getCurrentSession().update(userTest);
        return null;
    }

    @Override
    public void delete(int id) {
        UserTest userTest = sessionFactory.getCurrentSession().load(UserTest.class, id);
        if (userTest != null) {
            this.sessionFactory.getCurrentSession().delete(userTest);
        }
    }

    @Override
    public UserTest get(int id)  {
        return sessionFactory.getCurrentSession().get(UserTest.class, id);
    }

    @Override
    public List<UserTest> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from UserTest").list();
    }
}

package egs.dao.impl;


import egs.dao.UserDao;
import egs.entity.User;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoImp implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void add(User user) {
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }

    @Override
    public void delete(int id) {
        User user = sessionFactory.getCurrentSession().load(User.class, id);
        if (user != null) {
            this.sessionFactory.getCurrentSession().delete(user);
        }
    }

    @Override
    public List<User> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from user").list();
    }


    @Override
    public User update(User user) {
        sessionFactory.getCurrentSession().update(user);
        return user;
    }

    @Override
    public User get(int id) {
        return sessionFactory.getCurrentSession().get(User.class, id);
    }

    @Override
    public User getByEmail(String email) {
        Query query = sessionFactory.getCurrentSession().createQuery("from user where email = :email");
        query.setParameter("email", email);
        return (User) query.list().get(0);
    }
}

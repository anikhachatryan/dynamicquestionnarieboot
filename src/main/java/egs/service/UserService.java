package egs.service;

import egs.entity.User;

import java.util.List;

public interface UserService {

    void add(User user);

    User update(User user);

    void delete(int id);

    User get(int id);

    List<User> getAll();

    User getByEmail(String email);
}

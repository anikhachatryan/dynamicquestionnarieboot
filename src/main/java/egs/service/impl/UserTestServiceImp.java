package egs.service.impl;

import egs.dao.UserTestDao;
import egs.entity.UserTest;
import egs.service.UserTestService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UserTestServiceImp implements UserTestService {

    private UserTestDao userTestDao;

    @Autowired
    public UserTestServiceImp(UserTestDao userTestDao) {
        this.userTestDao = userTestDao;
    }

    @Override
    public void add(UserTest userTest) {
        userTestDao.add(userTest);
    }

    @Override
    public UserTest update(UserTest userTest) {
        return userTestDao.update(userTest);
    }

    @Override
    public void delete(int id) {
        userTestDao.delete(id);
    }

    @Override
    public UserTest get(int id) {
        return userTestDao.get(id);
    }

    @Override
    public List<UserTest> getAll() {
        return userTestDao.getAll();
    }
}

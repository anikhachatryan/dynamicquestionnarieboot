package egs.service.impl;

import egs.dao.QuestionDao;
import egs.entity.Question;
import egs.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class QuestionServiceImp implements QuestionService {

    private QuestionDao questionDao;

    @Autowired
    public QuestionServiceImp(QuestionDao questionDao) {
        this.questionDao = questionDao;
    }

    @Override
    @Transactional
    public void add(Question question) {
        questionDao.add(question);
    }

    @Override
    @Transactional
    public void delete(int id) {
        questionDao.delete(id);
    }

    @Override
    @Transactional
    public List<Question> getAll() {
        return questionDao.getAll();
    }

    @Override
    @Transactional
    public Question update(Question question) {
        return questionDao.update(question);
    }

    @Override
    @Transactional
    public Question get(int id) {
        return questionDao.get(id);
    }

    public void setQuestionDao(QuestionDao questionDao) {
        this.questionDao = questionDao;
    }
}

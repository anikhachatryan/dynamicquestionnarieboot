package egs.service.impl;

import egs.dao.TestDao;
import egs.entity.Test;
import egs.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TestServiceImp implements TestService {

    private TestDao testDao;

    @Autowired
    public TestServiceImp(TestDao testDao) {
        this.testDao = testDao;
    }

    @Override
    @Transactional
    public void add(Test test) {
        testDao.add(test);
    }

    @Override
    @Transactional
    public void delete(int id) {
        testDao.delete(id);
    }

    @Override
    @Transactional
    public List<Test> getAll() {
        return testDao.getAll();
    }

    @Override
    public List<Test> getAllByUser(Long id) {
        return testDao.getAllByUser(id);
    }

    @Override
    @Transactional
    public Test update(Test admin) {
        return testDao.update(admin);
    }

    @Override
    @Transactional
    public Test get(int id) {
        return testDao.get(id);
    }

}

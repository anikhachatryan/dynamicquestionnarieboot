package egs.service.impl;

import egs.dao.AdminDao;
import egs.entity.Admin;
import egs.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AdminServiceImp implements AdminService {

    private AdminDao adminDao;

    @Autowired
    public AdminServiceImp(AdminDao adminDao) {
        this.adminDao = adminDao;
    }

    @Override
    @Transactional
    public void add(Admin admin) {
        adminDao.add(admin);
    }

    @Override
    @Transactional
    public void delete(int id) {
        adminDao.delete(id);
    }

    @Override
    @Transactional
    public List<Admin> getAll() {
        return adminDao.getAll();
    }

    @Override
    public Admin getByEmail(String email) {
        return adminDao.getByEmail(email);
    }

    @Override
    @Transactional
    public Admin update(Admin admin) {
        return adminDao.update(admin);
    }

    @Override
    @Transactional
    public Admin get(int id) {
        return adminDao.get(id);
    }

}

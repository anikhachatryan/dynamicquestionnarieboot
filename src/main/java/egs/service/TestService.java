package egs.service;


import egs.entity.Test;

import java.util.List;

public interface TestService {
    void add(Test test);

    void delete(int id);

    Test update(Test test);

    Test get(int id);

    List<Test> getAll();

    List<Test> getAllByUser(Long id);
}

package egs.service;

import egs.entity.User;
import egs.entity.UserTest;

import java.util.List;

public interface UserTestService {

    void add(UserTest userTest);

    UserTest update(UserTest userTest);

    void delete(int id);

    UserTest get(int id);

    List<UserTest> getAll();
}
